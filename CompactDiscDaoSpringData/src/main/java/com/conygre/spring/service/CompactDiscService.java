package com.conygre.spring.service;

import com.conygre.spring.entities.CompactDisc;

import com.conygre.spring.data.repos.CompactDiscRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

@Service
@Transactional (propagation = Propagation.REQUIRED)
public class CompactDiscService {

	@Autowired
	private CompactDiscRepository repo;
	
	public Collection<CompactDisc> getCatalog()	{								// Retrieve
		return repo.findAll();
	}

	public CompactDisc getCompactDiscById(int id) {								// Retrieve
		// JPAPrepository only offer
		//     T getById(ID id)     so don't need to deal with Optional
		//  but it will throw an EntityNotFoundException if not found
		Optional<CompactDisc> opt = repo.findById(id);
		return opt.get();					// throws NoSuchElementException if not found
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)						// Create
	public void addToCatalog(CompactDisc disc) {
		repo.save(disc);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteFromCatalog(CompactDisc disc) {							// Delete
		repo.delete(disc);
	}

	public CompactDisc getCompactDiscByTitle(String title) {					// Retrieve
		return repo.findByTitle(title);
	}

	public Collection<CompactDisc> getCompactDiscsByArtist(String artist) {							// Retrieve
		return repo.findByArtist(artist);
	}
}
