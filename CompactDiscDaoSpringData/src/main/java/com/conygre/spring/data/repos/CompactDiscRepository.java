package com.conygre.spring.data.repos;

import com.conygre.spring.entities.CompactDisc;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface CompactDiscRepository extends JpaRepository<CompactDisc, Integer> {

    // generate my own custom queries
    public CompactDisc findByTitle(String title);
    public Collection<CompactDisc> findByArtist(String artist);
    public Collection<CompactDisc> findByArtistAndTitle(String artist, String title);

}
