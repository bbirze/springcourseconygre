import com.conygre.spring.entities.CompactDisc;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.conygre.spring.configuration.AppConfig;
import com.conygre.spring.service.CompactDiscService;

import java.util.Collection;


public class TestSpringDataJpa {
	private static final Logger log = Logger.getLogger(TestSpringDataJpa.class);

	public static void main(String[] args) {
		ApplicationContext context =
				new  AnnotationConfigApplicationContext(AppConfig.class);

		//  Can't autowire in service because TestSprignDataJpa is not a Spring bean
		CompactDiscService service = context.getBean(CompactDiscService.class);

		log.info("Retrieve All CDs: ");
		service.getCatalog().forEach(c -> System.out.println("\tCD: " + c.getTitle()));

		String title = "Welcome 2 America";
		String artist = "Prince";
		double price = 250.00;
		int tracks = 11;

		CompactDisc cd = new CompactDisc(title, price, artist, tracks);
		log.info("Add cd: " + cd.getTitle());
		service.addToCatalog(cd);

		log.info("Custom query: Retrieve cd by title: " + title);
		CompactDisc retCD = service.getCompactDiscByTitle(title);
		log.info("Retrieve cd by title got: " + retCD.getTitle() + " id: " + retCD.getId());
		int id = retCD.getId();

		log.info("Custom query: Retrieve cd by artist: " + artist);
		Collection<CompactDisc> retCDs = service.getCompactDiscsByArtist(artist);
		retCDs.forEach(c -> System.out.println(c.getTitle() + " id: " + c.getId()));

		log.info("Retrieve cd by ID: " + id);
		retCD = service.getCompactDiscById(id);
		log.info("Retrieve cd by ID: got: " + retCD.getTitle() + " id: " + retCD.getId());

		log.info("delete new cd: " + retCD.getTitle() + " id: " + retCD.getId());
		service.deleteFromCatalog(retCD);

		log.info("Retrieve All CDs: ");
		service.getCatalog().forEach(c -> System.out.println("\tCD: " + c.getTitle()));
	}

}
