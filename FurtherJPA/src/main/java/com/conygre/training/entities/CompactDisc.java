package com.conygre.training.entities;

import javax.persistence.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "compact_discs")
public class CompactDisc implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;
    @Column(name="title")
    private String title;
    private String artist;
    private Double price;

    @JoinColumn(name="cd_id", referencedColumnName = "id")
    @OneToMany(cascade={CascadeType.MERGE, CascadeType.PERSIST})
    private List<Track> trackTitles = new ArrayList<Track>();

    public CompactDisc()  {}                             // default constructor for Hibernate
                                                         // easier to use constructor for test code
    public CompactDisc(String title, String artist, Double price) {
        this.title = title;
        this.artist = artist;
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    @Column(name="artist")
    public String getArtist() {
        return artist;
    }

    @Column(name="price")
    public Double getPrice() {
        return price;
    }

    public Integer getId() {
        return id;
    }

    public List<Track> getTrackTitles() {
        return trackTitles;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setTrackTitles(List<Track> trackTitles) {
        this.trackTitles = trackTitles;
    }
}

