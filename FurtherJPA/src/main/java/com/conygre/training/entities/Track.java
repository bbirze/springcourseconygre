package com.conygre.training.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tracks")
public class Track implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;
    @Column(name="title")
    private String title;
    @Column(name="cd_id")           // FK mapped to Compact Disc id
    private Integer cdId;

    public Integer getId() {   return id; }

    public String getTitle() {
        return title;
    }
    public Integer getCdId() { return cdId;  }

    public void setId(Integer id) {
        this.id = id;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public void setCdId(Integer cdId) {
        this.cdId = cdId;
    }

}
