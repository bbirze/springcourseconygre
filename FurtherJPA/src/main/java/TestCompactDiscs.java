import com.conygre.training.entities.CompactDisc;

import javax.persistence.*;

import org.apache.log4j.Logger;

public class TestCompactDiscs {
    private static Logger log = Logger.getLogger(TestCompactDiscs.class);

    public static void main(String[] args) {

        EntityManagerFactory factory = 
                Persistence.createEntityManagerFactory("ConygrePersistentUnit");
        EntityManager em = factory.createEntityManager();

        EntityTransaction tx = em.getTransaction();
        tx.begin();

        // get all discs and log them
        log.info("\n\tRetrieve all the CD");                            // Retrieve All CDs
        Query query = em.createQuery("from CompactDisc");
        java.util.List<CompactDisc> cds = query.getResultList();
        for(CompactDisc d: cds)  {
            log.info("The CD ["+  d.getId() +"] is " + d.getTitle());
        }
        
        // ====  All tracks from CD 16 ===
        log.info("\n\tRetrieve CD with id=16");                         // Retrieve CD with id=11
        CompactDisc disc16 = em.find(CompactDisc.class, 16);
        log.info("Got Disk: " + disc16.getTitle());                     // log title
        Query allSpiceGirlTracks = em.createQuery(
                "select t.title from Track t where t.cdId = 16");

//        allSpiceGirlTracks.getResultList().forEach((x)->{log.info(x);});
        java.util.List<String> tracks = allSpiceGirlTracks.getResultList();
        for(String t: tracks)  {
            log.info("\t track:" + t);
        }
        
        // ====  All CDs where artist name starts with 'S' ===
        log.info("\n\tRetrieve all CD with Artist name starts with S");
        Query startWithS = em.createQuery(
                "select c.artist from CompactDisc c where c.artist LIKE 'S%'");
        java.util.List<String> artists = startWithS.getResultList();
        for(String a: artists)  {
            log.info("\t artist:" + a);
        }
        
        // ==== Number of CDs in database  ===  
        log.info("\n\tRetrieve Number of CDs");
        Query cntQuery = em.createQuery("select count(*) from CompactDisc");
        Long num = (Long)cntQuery.getSingleResult();                    // Query returns generic Object
        log.info("\tNumber of CDs in database: " + num);

        // ==== All CDs alphabetically sorted  ===  
        log.info("\n\tRetrieve all CDs sorted alphabetically");
        query = em.createQuery("select cd from CompactDisc as cd order by cd.title");
        cds = query.getResultList();
        for(CompactDisc c : cds)  {
            log.info("\tCD ["+  c.getId() +"]: " + c.getTitle());
        }

        // ==== All Spice Girls CDs Typed Query  ===  
        String artist = "Spice Girls";
        log.info(String.format("\n\tRetrieve CD Artist: %s", artist));
        TypedQuery<CompactDisc> tQquery =
                em.createQuery("select c from CompactDisc c where c.artist = :artist",
                        CompactDisc.class);
        tQquery.setParameter("artist", artist);
        CompactDisc SpiceCd = tQquery.getSingleResult();
        log.info("\tCD: " + SpiceCd.getTitle() + " by artist " + SpiceCd.getArtist());
 
        // ====  All tracks by Spice Girls using simple FK Mapping ===  
        Integer id = SpiceCd.getId();                                    // Spice Girls cds, using simple FK Mapping
        log.info(String.format("\n\tRetrieve track titles from CD with ID: %s", id));
        Query allSpiceGirlTracksQuery =
                em.createQuery("select t.title from Track t where t.cdId  = :id")
                        .setParameter("id", id);

        allSpiceGirlTracksQuery.getResultList().forEach((x)->{log.info(x);});

//       log.info("\n\tUpdated CD List! ");
//        cds = query.getResultList();                                    // get CDs again
//        for(CompactDisc d: cds)  {
//            log.info("The CD ["+  d.getId() +"] is " + d.getTitle());
//        }
        tx.commit();
        em.close();
    }
}
