import com.conygre.training.entities.CompactDisc;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class LifeCycleTest {
    private static Logger log = Logger.getLogger(LifeCycleTest.class);

    public static void main(String[] args) {

        EntityManagerFactory factory = 
                Persistence.createEntityManagerFactory("ConygrePersistentUnit");
        EntityManager em = factory.createEntityManager();

        // ====  EntityManager Transaction 1 ===
        EntityTransaction tx = em.getTransaction();
        log.info("\n\tBegin Transaction with EntityManager 1" );
        tx.begin();

        Integer id = 12;
        log.info(String.format("\tRetrieve CD with id: %d", id));
 
        CompactDisc disc = em.find(CompactDisc.class, 12);
        log.info(String.format("\tRetrieved CD [%d] Title: %s ", disc.getId(), disc.getTitle()));

        String title = "Updated:" + disc.getTitle();
        log.info("\tUpdating title to ["+  title +"]  " );
        disc.setTitle(title);                                       // persist new title to db
    
        CompactDisc cdChanged = em.find(CompactDisc.class, id);     // retrieve from db again
        log.info(String.format("\tRetrieved updated CD [%d] Title: %s ", cdChanged.getId(), cdChanged.getTitle()));

        log.info("\tCommit and Close EntityManager 1" );
        tx.commit();
        em.close();

        // ====  No EntityManager or Transaction  ===
        title = "another thing entirely";
        log.info(String.format("\tUpdating local CompactDisc instance title to [%s] After EntityManager closed ", title));
        disc.setTitle(title);                                          // unmanaged disc

        // ====  EntityManager Transaction 2 ===
        log.info("\n\tBegin Transaction with EntityManager 2" );
    
        EntityManager em2 = factory.createEntityManager();
        tx = em2.getTransaction();
        tx.begin();

        em2.merge(disc);                                                // merge unmanaged disc
        em2.flush();                                                    // flush changes to db
        CompactDisc disc2 = em2.find(CompactDisc.class, id);            // retrieve disc from dbs
        log.info(String.format("\tRetrieved updated CD [%d] Title: %s ", disc2.getId(), disc2.getTitle()));

        log.info("\tCommit and Close EntityManager 2" );
        tx.commit();
        em2.close();
    }
}
