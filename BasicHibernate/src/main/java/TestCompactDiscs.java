import com.conygre.training.entities.CompactDisc;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.log4j.Logger;

public class TestCompactDiscs {
    private static Logger log = Logger.getLogger(TestCompactDiscs.class);

    public static void main(String[] args) {
                                                                        // Create entity manager
        EntityManagerFactory factory =
                Persistence.createEntityManagerFactory("ConygrePersistentUnit");
        EntityManager em = factory.createEntityManager();

        EntityTransaction tx = em.getTransaction();                     // get and start transaction
        tx.begin();

        log.info("\n\tRetrieve ALL cds");                       // ******* Retrieve ALL Cds
        Query query = em.createQuery("from CompactDisc");        // get all discs and log them
        java.util.List<CompactDisc> cds = query.getResultList();        // returns raw List
        for(CompactDisc d: cds)  {
            log.info("The CD ["+  d.getId() +"] is " + d.getTitle());
        }

        log.info("\n\tUpdate title on CD with id=11");          // ******* Get and update CD with id=11

        CompactDisc disc11 = em.find(CompactDisc.class, 11);   // Get cd with id 11
        disc11.setTitle("A Rush of Blood to the Head");                 // update title
        disc11 = em.find(CompactDisc.class, 11);               // get it from DB again
        log.info("Updated Disk with id 11: " + disc11.getTitle());      // log title

        log.info("\n\tAdd new CD");          // ******* add new disc

        CompactDisc newOne = new CompactDisc("Piano Man", "Billy Joel", 25.99);
        em.persist(newOne);                                             // Add new CD

        log.info("Added CD " + newOne.getTitle() + " by " + newOne.getArtist());

        log.info("\n\tUpdated CD List! ");
        cds = query.getResultList();                                    // get CDs again
        for(CompactDisc d: cds)  {
            log.info("The CD ["+  d.getId() +"] is " + d.getTitle());
        }
        tx.commit();
        em.close();
    }
}
