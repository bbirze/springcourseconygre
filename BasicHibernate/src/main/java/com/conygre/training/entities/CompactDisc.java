package com.conygre.training.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity                                                  // look at me!  I'm an Entity
@Table(name = "compact_discs")                           // for the DB table compact_discs
public class CompactDisc implements Serializable {       // Java Beans implement Serializable I/F
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)  // mySql wants the IDENTITY strategy for auto-generated PK
    @Column(name="id")
    private Integer id;
    private String title;  
    private String artist;
    private Double price; 


    public CompactDisc()  {}                             // default constructor for Hibernate
                                                         // easier to use constructor for test code
    public CompactDisc(String title, String artist, Double price) {
        this.title = title;
        this.artist = artist;
        this.price = price;
    }

    @Column(name="title")                                   // Annotate column mapping on getters
    public String getTitle() {
        return title;
    }

    @Column(name="artist")
    public String getArtist() {
        return artist;
    }

    @Column(name="price")
    public Double getPrice() {
        return price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
