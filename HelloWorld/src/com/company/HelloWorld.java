package com.company;

public class HelloWorld {

    public static void main(String[] args) {
        sayHello("BB");
    }

    public static void sayHello(String name)  {
        System.out.println(("Hello " + name));
    }
}
