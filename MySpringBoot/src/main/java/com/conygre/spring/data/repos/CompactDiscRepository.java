package com.conygre.spring.data.repos;

import com.conygre.spring.entities.CompactDisc;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CompactDiscRepository extends JpaRepository<CompactDisc, Integer> {
}
