package com.conygre.spring.rest;

import com.conygre.spring.entities.CompactDisc;
import com.conygre.spring.service.CompactDiscService;
import com.conygre.spring.service.CompactDiscServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@RestController
@RequestMapping("/api/compactdiscs")
public class CompactDiscController {
    private static final Logger logger = LogManager.getLogger(CompactDiscController.class);

    @Autowired
    private CompactDiscService service;

    @RequestMapping(method = RequestMethod.GET)
    List<CompactDisc> findAll()  {
        return service.getCatalog();
    }

    @GetMapping(value="/{id}")
    public CompactDisc getCompactDiscById(@PathVariable("id") int id)  {
        logger.info("got id: " + id);
        return service.getCompactDiscById(id);
    }

    @PostMapping(consumes="application/json")
    public CompactDisc addCompactDiscById(@RequestBody CompactDisc disc)  {
        logger.info("got cd: " + disc);
        return service.addToCatalog(disc);
    }

    @PutMapping(consumes="application/json")
    public CompactDisc updateCompactDiscById(@RequestBody CompactDisc disc)  {
        logger.info("got cd: " + disc);
        if (service.getCompactDiscById(disc.getId()) != null) {
            logger.info("disc exists, update");
            return service.updateCompactDisc(disc);
        }
        logger.info("disc does not exists, add it");
        return service.addToCatalog(disc);
    }

    @DeleteMapping(value="/{id}")
    public void deleteCompactDiscById(@PathVariable("id") int id)  {
        logger.info("got id: " + id);
        service.deleteCompactDiscById(id);
    }

    @DeleteMapping(consumes="application/json")
    public void deleteCompactDisc(@RequestBody CompactDisc disc)  {
        logger.info("got cd: " + disc);
        service.deleteCompactDisc(disc);
    }



}
