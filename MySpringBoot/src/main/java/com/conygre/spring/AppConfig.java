package com.conygre.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(SwaggerConfig.class)
public class AppConfig {

    public static void main(String[] args)  {
        SpringApplication.run(AppConfig.class, args);
    }
}
