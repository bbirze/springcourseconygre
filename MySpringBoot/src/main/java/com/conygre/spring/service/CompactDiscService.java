package com.conygre.spring.service;

import com.conygre.spring.entities.CompactDisc;

import java.util.List;

public interface CompactDiscService {

	List<CompactDisc> getCatalog();
	public CompactDisc getCompactDiscById(int id);
	public CompactDisc addToCatalog(CompactDisc disc);
	public CompactDisc updateCompactDisc(CompactDisc disc);
	public void deleteCompactDiscById(int id);

	void deleteCompactDisc(CompactDisc disc);
}
