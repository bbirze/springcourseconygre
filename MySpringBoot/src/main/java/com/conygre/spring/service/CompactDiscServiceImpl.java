package com.conygre.spring.service;

import com.conygre.spring.data.repos.CompactDiscRepository;
import com.conygre.spring.entities.CompactDisc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Service
@Transactional (propagation = Propagation.REQUIRED)
public class CompactDiscServiceImpl implements CompactDiscService {

	private static final Logger logger = LogManager.getLogger(CompactDiscServiceImpl.class);

	@Autowired
	private CompactDiscRepository repo;

	@Override
	public List<CompactDisc> getCatalog() {
		logger.info("getting the catalog");
		return repo.findAll();
	}

	@Override
	public CompactDisc getCompactDiscById(int id) {
		Optional<CompactDisc> opt = repo.findById(id);
		if(opt.isPresent()) {
			return opt.get();
		}
		return null;
	}

	@Override
	@Transactional (propagation = Propagation.REQUIRES_NEW)
	public CompactDisc addToCatalog(CompactDisc disc) {
		logger.info("Adding " + disc);
		return repo.save(disc);
	}

	@Override
	public CompactDisc updateCompactDisc(CompactDisc disc) {
		logger.info("Updating " + disc + " - insert if not found");
		return repo.save(disc);		// insert if not found to update
	}

	@Override
	public void deleteCompactDiscById(int id) {
		logger.info("Delete CD id:" + id);
		CompactDisc deleteCD = repo.findById(id).get();
		deleteCompactDisc(deleteCD);
	}

	@Override
	public void deleteCompactDisc(CompactDisc disc) {
		logger.info("deleting " + disc);
		repo.delete(disc);
	}

}
