package com.conygre.test.address;

import org.springframework.stereotype.Component;

@Component
public class Address {

    public String toString()  {
        return "Address class here!";
    }

}
