package com.conygre.test.address;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan        // defaults to current package and all child packages
public class AddressConfiguration {}
