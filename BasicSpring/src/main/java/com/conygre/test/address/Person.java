package com.conygre.test.address;

import com.conygre.test.pets.Pet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class Person {

    @Autowired
    private Address address;

     public Person(Address addr) {
        this.address = addr;
    }
    public Person(){}

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address addr) {
        this.address = addr;
    }
}
