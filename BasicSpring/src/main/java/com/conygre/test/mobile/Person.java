package com.conygre.test.mobile;

public class Person {
    private Mobile cell;

    public Person(Mobile c) {
        this.cell = c;
    }
    public Person(){}

    public Mobile getMobile() {
        return cell;
    }

    public void setCell(Mobile c) {
        this.cell = c;
    }
}
