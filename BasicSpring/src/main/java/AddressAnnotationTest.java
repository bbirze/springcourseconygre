import com.conygre.test.address.Person;
import com.conygre.test.address.AddressConfiguration;
import com.conygre.test.address.Address;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AddressAnnotationTest {

    public static void main(String[] args) {
        ApplicationContext context =
                new AnnotationConfigApplicationContext(AddressConfiguration.class);

        Person person = context.getBean(Person.class);
        System.out.println("Got person by class name: " + person);

        Address address = person.getAddress();
        System.out.println("\t with address: "  + address);


            // Spring Beans default to Singleton, so will be the same object
        Person person2 = (Person)context.getBean("person");
        System.out.println("Got person by bean name: " + person2);

        address = person2.getAddress();
        System.out.println("\t with address: "  + address);
    }
}
