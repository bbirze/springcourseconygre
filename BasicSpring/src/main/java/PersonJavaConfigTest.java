import com.conygre.test.pets.Person;
import com.conygre.test.pets.PersonConfiguration;
import com.conygre.test.pets.Pet;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class PersonJavaConfigTest {

    public static void main(String[] args) {
        ApplicationContext context =
                new AnnotationConfigApplicationContext(PersonConfiguration.class);

        Person person = context.getBean(Person.class);
        System.out.println("Got person by class name: " + person);

        Pet pet = person.getPet();
        pet.feed();

        Person person2 = (Person)context.getBean("person");
        System.out.println("Got person by bean name: " + person2);

        pet = person2.getPet();
        pet.feed();

        // Spring Beans default to Singleton, so will be the same object
        if (person == person2)  {
            System.out.println("Got same bean from both getBean calls!");
        }  else {
            System.out.println("Spring beans person and person2 are different objects");
        }
    }
}
