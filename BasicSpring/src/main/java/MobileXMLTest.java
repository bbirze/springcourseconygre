import com.conygre.test.mobile.Mobile;
import com.conygre.test.mobile.Person;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MobileXMLTest {

    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("mobile.xml");

        Person person = context.getBean(Person.class);
        System.out.println("Got person by class name: " + person);

        Mobile cell = person.getMobile();
        System.out.println("\t with cell: "  + cell);


            // Spring Beans default to Singleton, so will be the same object
        Person person2 = (Person)context.getBean("person");
        System.out.println("Got person by bean name: " + person2);

        cell = person2.getMobile();
        System.out.println("\t with cell: "  + cell);
    }
}
