package functional.tests;

import com.conygre.spring.boot.entities.CompactDisc;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TestRestTemplateFctTest {

//    @Autowired
//    private TestRestTemplate restTemplate;
//
//    @Test
//    public void testFindAll() {
//        List<CompactDisc> cds =
//                this.restTemplate.getForObject("/api/compactdiscs", List<>.class>);
//        assertThat(cds.size(), greaterThan(1));
//    }
//
//    @Test
//    public void testCdById() {
//        CompactDisc cd = restTemplate.getForObject("/compactdiscs/16", CompactDisc.class);
//        assertThat(cd.getArtist(), equalTo("Spice Girls"));
//    }
}
