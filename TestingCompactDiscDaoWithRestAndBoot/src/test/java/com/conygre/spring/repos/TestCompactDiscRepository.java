package com.conygre.spring.repos;

import com.conygre.spring.boot.entities.CompactDisc;
import com.conygre.spring.boot.repos.CompactDiscRepository;
import com.conygre.spring.boot.rest.CompactDiscController;
import com.conygre.spring.boot.services.CompactDiscService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;	    // static import typically used
import static org.hamcrest.Matchers.equalTo;;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@RunWith(SpringRunner.class)
@DataJpaTest                    // config in-memory embedded db for @Entity classes and SPring Data JPA repo
// uncomment only if don'g want transaction to not rollback  at end of each test
//@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class TestCompactDiscRepository {

    @Autowired
    private TestEntityManager manager;

    @Autowired
    private CompactDiscRepository repo;

    @Autowired
    CompactDiscService discService;

    @Autowired
    CompactDiscController controller;

    private int discId;

    @Before
    public  void setupDatabaseEntryForReadOnlyTests() {
        CompactDisc disc = new CompactDisc("Abba Gold", 12.99, "Abba", 5);
        CompactDisc result = manager.persist(disc);
        discId = result.getId();
    }
    @Test
    public void canRetrieveCDByArtist() {
        Iterable<CompactDisc> discs = repo.findByArtist("Abba");
        Stream<CompactDisc> stream = StreamSupport.stream(discs.spliterator(), false);
        assertThat(stream.count(), equalTo(1L));
    }
    @Test
    public void compactDiscServiceCanReturnACatalog() {
        Iterable<CompactDisc> discs = discService.getCatalog();
        Stream<CompactDisc> stream = StreamSupport.stream(discs.spliterator(), false);
        Optional<CompactDisc> firstDisc = stream.findFirst();
        assertThat(firstDisc.get().getArtist(), equalTo("Abba"));
    }
    @Test
    public void controllerCanReturnCDById() {
        CompactDisc cd = controller.getCdById(discId);
        assertThat(cd.getArtist(), equalTo("Abba"));
    }
}
