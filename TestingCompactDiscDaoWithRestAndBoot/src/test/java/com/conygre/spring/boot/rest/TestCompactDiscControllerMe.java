package com.conygre.spring.boot.rest;

import com.conygre.spring.boot.AppConfig;
import com.conygre.spring.boot.entities.CompactDisc;
import com.conygre.spring.boot.repos.CompactDiscRepository;
import com.conygre.spring.boot.services.CompactDiscService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;


import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;	// static import typically used
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=(AppConfig.class))	    // init context and initialize Spring Boot
@WebMvcTest(CompactDiscController.class)
//xtConfiguration(classes={com.conygre.spring.boot.AppConfig.class})
public class TestCompactDiscControllerMe {
    @Autowired
    private CompactDiscController controller;

    @MockBean
    private CompactDiscService service;

    @MockBean
    CompactDiscRepository repo;

    @Test
    public void testFindAll()  {
        Iterable<CompactDisc> cds = controller.findAll();
        Stream<CompactDisc> stream = StreamSupport.stream(cds.spliterator(), false );
        assertThat(stream.count(), equalTo(1L));
    }

    @Test
    public void testCdById() {
        CompactDisc cd = controller.getCdById(1);
        assertNotNull(cd);
    }

    //    @TestConfiguration
//    protected static class Config {
//        @Bean
//        @Primary
//        public CompactDiscRepository repo() {
//            return mock(CompactDiscRepository.class);
//        }
//        @Bean
//        @Primary
//        public CompactDiscService service() {
//            CompactDisc cd = new CompactDisc();
//            List<CompactDisc> cds = new ArrayList<>();
//            cds.add(cd);
//
//            CompactDiscService service = mock(CompactDiscService.class);
//            when(service.getCatalog()).thenReturn(cds);
//            when(service.getCompactDiscById(1)).thenReturn(cd);
//            return service;
//        }
//        @Bean
//        @Primary
//        public CompactDiscController controller() {
//            return new CompactDiscController();
//        }
//    }                                   // end of Config nested static class

}


