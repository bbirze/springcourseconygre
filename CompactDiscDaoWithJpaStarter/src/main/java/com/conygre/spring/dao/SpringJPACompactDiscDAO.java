package com.conygre.spring.dao;

import java.util.Collection;
import java.util.List;

import javax.persistence.*;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import com.conygre.spring.dao.CompactDiscDAO;
import com.conygre.spring.entities.CompactDisc;



@Repository
@Transactional (propagation = Propagation.SUPPORTS)
public class SpringJPACompactDiscDAO implements CompactDiscDAO{

    private static final Logger log = Logger.getLogger(SpringJPACompactDiscDAO.class);

    @PersistenceContext                     // inject our Entity Manager
    EntityManager em;

    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    @Override
    public void addCompactDisc(CompactDisc disc) {
        log.debug("add disk: " + disc);
        em.persist(disc);
    }

    @Override
    public CompactDisc getCompactDiscByTitle(String title) {
        log.debug("Find by title, given: " + title);
        TypedQuery<CompactDisc> query =
                em.createQuery("select c from CompactDisc c where c.title = :title",
                        CompactDisc.class);
        query.setParameter("title", title);
        CompactDisc cd = query.getSingleResult();
        log.debug("retrieved CD " + cd.getTitle() + " by artist " + cd.getArtist());
        return cd;
    }

    @Override
    public Collection<CompactDisc> getDiscsByArtist(String artist) {
        log.debug("find by artis, given: " + artist);
        TypedQuery<CompactDisc> query =
                em.createQuery("select c from CompactDisc c where c.artist = :artist",
                        CompactDisc.class);
        query.setParameter("artist", artist);
        List<CompactDisc> cds = query.getResultList();
        log.debug("retrieved CDs " + cds);
        return cds;
    }

    @Override
    public Collection<CompactDisc> getAllDiscs() {
        log.debug("get all discs ");
        Query query = em.createQuery("from CompactDisc");
        List<CompactDisc> discs = query.getResultList();
        log.debug("retrieved discs " + discs);
        return discs;
    }

    @Override
    public CompactDisc getCompactDiscById(int id) {
        log.debug("find by id, given: " + id);
        CompactDisc cd = em.find(CompactDisc.class, id);
        log.debug("retrieved CD ["+ cd.getId() +"] with title " + cd.getTitle()+ ", by artist " + cd.getArtist());

        return cd;
    }
}
