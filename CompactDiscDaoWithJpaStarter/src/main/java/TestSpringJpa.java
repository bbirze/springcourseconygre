import com.conygre.spring.entities.CompactDisc;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.conygre.spring.configuration.AppConfig;
import com.conygre.spring.service.CompactDiscService;

import java.util.Collection;


public class TestSpringJpa {
	private static final Logger log = Logger.getLogger(TestSpringJpa.class);

	public static void main(String[] args) {
		ApplicationContext context =
				new  AnnotationConfigApplicationContext(AppConfig.class);
		CompactDiscService service = context.getBean(CompactDiscService.class);

		log.info("Retrieve All CDs: ");
		service.getCatalog().forEach(c -> log.info("\t" + c.getTitle()));

		String title = "Welcome 2 America";
		String artist = "Prince";
		double price = 250.00;
		int tracks = 11;

		CompactDisc cd = new CompactDisc(title, price, artist, tracks);
		log.info("Add CD [" + cd.getId()+ "]: " + cd.getTitle());
		service.addToCatalog(cd);

		log.info("Retrieve cd by title: " + title);
		CompactDisc retCD = service.getCompactDiscByTitle(title);
		log.info("\tGot CD [" + retCD.getId()+ "]: " + retCD.getTitle());
		int id = retCD.getId();

		log.info("Retrieve cd by artist: " + artist);
		Collection<CompactDisc> retCDs = service.getCompactDiscsByArtist(artist);
		log.info("\tGot cds by artist " + artist +": ");
		for(CompactDisc d : retCDs)  {
			log.info("\tCD [" + d.getId()+"]: " + d.getTitle());
		}

		log.info("Retrieve cd by ID: " + id);
		retCD = service.getCompactDiscById(id);
		log.info("\tGot CD [" + retCD.getId()+ "]: " + retCD.getTitle());

		log.info("Retrieve All CDs:");
		retCDs= service.getCatalog();
		service.getCatalog().forEach(c -> log.info("\t" + c.getTitle()));
	}

}
